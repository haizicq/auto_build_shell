# 百度到的有用文章
* [SSH免登陆配置](http://blog.csdn.net/baight123/article/details/51023556)
* [sed命令说明](sed命令说明.md)
* [Shell字符串处理、文件名、后缀名处理](http://blog.csdn.net/guojin08/article/details/38704823)
* [Shell参考手册](https://tiswww.case.edu/php/chet/bash/bashref.html#Pipelines)
* [Banner生成](http://patorjk.com/software/taag/#p=testall&f=Bulbhead&t=ATBSHELL)
* [编写快速安全 Bash 脚本的建议](https://www.oschina.net/translate/bash-scripting-quirks-safety-tips)
* [Linux命令搜索](http://linux-command.composer-proxy.org/c/find.html)
* [Linux系统命令大全](http://man.linuxde.net/)
